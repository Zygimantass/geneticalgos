from Individual import Individual
from Population import Population
import Algorithm
from FitnessCalc import FitnessCalc
import time

fitnessCalc = FitnessCalc()

fitnessCalc.setSolution([int(i) for i in "11101110001111001111"])
pop = Population(20, True, fitnessCalc)

generationCount = 0

while pop.getFittest().getFitness(fitnessCalc) < fitnessCalc.getMaxFitness():
    generationCount += 1
    print "Generation: {0}; Fittest: {1}".format(generationCount, pop.getFittest())
    time.sleep(5)
    pop = Algorithm.evolvePopulation(pop, fitnessCalc)

print "Solution found"
print "Generation count: " + str(generationCount)
print "Genes:"
print pop.getFittest()
