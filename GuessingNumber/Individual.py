import random
import math
from FitnessCalc import FitnessCalc


class Individual:
    def __init__(self):
        self.defaultGeneLength = 20
        self.genes = []
        self.fitness = 0

    def generate(self):
	random.seed()
	for i in range(self.defaultGeneLength):
            self.genes.append(int(round(random.random())))
    def setDefaultGeneLength(length):
        self.defaultGeneLength = length

    def getGene(self, index):
        return self.genes[index]

    def setGene(self, index, gene):
        self.genes.append(gene)

    def getSize(self):
        return len(self.genes)

    def getFitness(self, fitnessCalc):
        if (self.fitness == 0):
            self.fitness = fitnessCalc.getFitness(self)
        return self.fitness

    def __str__(self):
        return "".join(map(str, self.genes))
