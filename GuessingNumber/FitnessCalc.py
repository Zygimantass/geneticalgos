class FitnessCalc:
    def __init__(self):
        self.solution = []
    def setSolution(self, newSolution):
        self.solution=newSolution

    def getFitness(self, indiv):
        return len(set(indiv.genes) & set(self.solution))

    def getMaxFitness(self):
        return len(self.solution)
