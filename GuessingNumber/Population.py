from Individual import Individual

class Population:
    def __init__(self, popSize, initialise, fitnessCalc):
        self.individuals = []
        self.fitnessCalc = fitnessCalc
        if initialise:
            for i in range(popSize):
                    individual = Individual()
                    individual.generate()
                    self.individuals.append(individual)
    def getIndividual(self, index):
        return self.individuals[index]
    def saveIndividual(self, index, ind):
        self.individuals += [ind]
    def getAllIndividuals(self):
	return self.individuals
    def getFittest(self):
        maxFitness = -1
        fittest = None
        for i in self.individuals:
            if i.getFitness(self.fitnessCalc) > maxFitness:
                maxFitness = i.getFitness(self.fitnessCalc)
                fittest = i
        return fittest
    def getSize(self):
        return len(self.individuals)
