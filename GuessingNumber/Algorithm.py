from Population import *
from Individual import *
import random
import math

uniformRate = 0.5
mutationRate = 0.015
tournamentSize = 2
elitism = True

def evolvePopulation(population, fitnessCalc):
    pop = Population(population.getSize(), False, fitnessCalc)
    if elitism:
        pop.saveIndividual(0, population.getFittest())
        elitismOffset = 1
    else:
        elitismOffset = 0

    for i in range(elitismOffset, population.getSize()):
        indiv1 = tournamentSelection(population, fitnessCalc)
        indiv2 = tournamentSelection(population, fitnessCalc)
	print indiv1, indiv2
        newIndiv = crossover(indiv1, indiv2)
        pop.saveIndividual(i, newIndiv)
    for i in range(elitismOffset, pop.getSize()):
        mutate(pop.getIndividual(i))

    return pop

def crossover(indiv1, indiv2):
    newSol = Individual()

    for i in range(indiv1.getSize()):
        if random.random() <= uniformRate:
            newSol.setGene(i, indiv1.getGene(i))
        else:
            newSol.setGene(i, indiv2.getGene(i))
    return newSol

def mutate(indiv):
    tempGenes = indiv.genes
    indiv.genes = []
    for i in range(indiv.getSize()):
        if random.random() <= mutationRate:
            indiv.setGene(i, int(round(random.random())))
	else:
	    indiv.setGene(i, tempGenes[i]) 
def tournamentSelection(population, fitnessCalc):
    tournament = Population(tournamentSize, False, fitnessCalc)
    for i in range(tournamentSize):
        randomId = int(random.random() * population.getSize() - 1)
        tournament.saveIndividual(None, population.getIndividual(int(randomId)))
    fittest = tournament.getFittest()
    del tournament
    return fittest
